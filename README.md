#	Raspberry Pi GPS Project
This project uses GPS module gt-7u on a raspberry pi.

##	Pre-requisites
- Python3
- python package "gps3" (NOT "gps")
- GPS gt-7u module

##	Programs
- marker.py
- gps.py

##	Program Description
- marker.py: designed to implement a static marker with GPS location
- gps.py: designed to track its current location and will compare from the output received from the marker. If the GPS module is in a certain range describe in the marker, it will send a message.

###	Sources
- gps3 package (https://pypi.org/project/gps3/)
- configuring gps module (https://maker.pro/raspberry-pi/tutorial/how-to-use-a-gps-receiver-with-raspberry-pi-4)


