#!/usr/bin/python
# marker.py: designed to implement a static marker with GPS location

import os
from gps3 import gps3
from time import *
import time
import threading
def average(cordData):
	min = cordData[0]
	max = cordData[0]
	for i in range(0,len(cordData)):
		if cordData[i] < min:
			min = cordData[i]
		if cordData[i] > max:
			max = cordData[i]
	return ((max+min)/2)

def marker():
	gps_socket = gps3.GPSDSocket()
	data_stream = gps3.DataStream()
	gps_socket.connect()
	gps_socket.watch()
	count = 0
	longDataArray = []
	latDataArray = []
	for new_data in gps_socket:
		if count >= 10:
			break	
		if new_data :
			data_stream.unpack(new_data)
			if data_stream.TPV['alt'] != 'n/a':
				long_data = data_stream.TPV['lon']
				longDataArray.append(long_data)
				print('longitude #'+ str(count+1) +' = '+ str(long_data))
				lat_data = data_stream.TPV['lat']
				latDataArray.append(lat_data)	
				print('Latitude #'+ str(count+1)+' = '+ str(lat_data))
				count = count + 1
	longAverage = average(longDataArray)
	print("The result is " + str(longAverage))
	latAverage = average(latDataArray)
	print("The result is " + str(latAverage))
	fptr = open("marker.txt", "w")
	fptr.write(str(longAverage)+ "\n")
	fptr.write(str(latAverage)+ "\n")
	fptr.close()

os.system('clear')
print("Implementing GPS marker...\n")
marker()
print("Marker implemented. Exiting Now!")
