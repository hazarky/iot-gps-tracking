#!/usr/bin/python
import os
from time import *
from gps3 import gps3
import time
import threading

def tracker():
    try:
        print("Starting socket and DataStream")
        gps_socket = gps3.GPSDSocket()
        data_stream = gps3.DataStream()
        gps_socket.connect()
        gps_socket.watch()
        fptr = open("marker.txt", "r")
        longData = fptr.readline()
        latData = fptr.readline()
        fptr.close()
        count = 0
        for new_data in gps_socket:
            if new_data:
                data_stream.unpack(new_data)
                if data_stream.TPV['alt'] != 'n/a':
                    newLongData = data_stream.TPV['lon']
                    newLatData = data_stream.TPV['lat']
#                     if (float(longData) - 0.0001) < newLongData < (float(longData) + 0.0001) and (float(latData) - 0.001) < newLatData < (float(latData) + 0.001):
                    if (float(longData) - 0.001) < newLongData < (float(longData) + 0.001) and (float(latData) - 0.001) < newLatData < (float(latData) + 0.001):  
                        print(str(count+1)+". Car is in the garage!!!")
                        count = count + 1
                    else:
                        print("Car is NOT in the garage")
    except (KeyboardInterrupt, SystemExit): #when you press ctrl+c
        print("Killing thread\n") 
        print("Done.\nExiting")
        

os.system('clear')
print("Starting GPS tracker...")
tracker()
print("Process done, Exiting\n")
